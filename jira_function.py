from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import NoSuchElementException

def jira_function_fail_filter(month, file_name, error):
    try:
        #print("entered jira pass function")
        s=Service(ChromeDriverManager().install())
        driver = webdriver.Chrome(service=s)
        driver.implicitly_wait(60)
        driver.maximize_window()
        sleep(1)
        driver.get("https://jmadevteam.atlassian.net/browse/"+file_name.split("\\")[-1])
        sleep(1)
        username_email_input = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div[2]/form/div[1]/div/div/div/input")
        username_email_input.click()
        username_email_input.send_keys("pselvarajan@blueally.com")
        continue_click = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div[2]/form/div[3]/button/span/span").click()
        password_email_input = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div[2]/form/div[2]/div/div/div/div/div/input")
        password_email_input.click()
        password_email_input.send_keys("Marines*012")
        login_click = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div[2]/form/div[3]/button/span").click()
        sleep(10)
        try:
            scr1 = driver.find_element_by_xpath('/html/body/div[5]/div[1]/div/div[3]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]')
        except NoSuchElementException:
            scr1 = driver.find_element_by_xpath('/html/body/div[1]/div[1]/div/div[3]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]')
        sleep(1)
        driver.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", scr1)
        #print("scrolled ##################")

        try:
            driver.switch_to.frame(driver.find_element_by_xpath("/html/body/div[5]/div[1]/div/div[3]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/div/div[5]/div[10]/div[3]/div[2]/div/iframe"))
        except NoSuchElementException:
            driver.switch_to.frame(driver.find_element_by_xpath("/html/body/div[1]/div[1]/div/div[3]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/div/div[5]/div[10]/div[3]/div[2]/div/iframe"))
        sleep(4)

        page_extend = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div[2]/div[1]/div/div[3]").click()
        sleep(2)
        temp_list = []
        temp_list = driver.find_elements_by_xpath("/html/body/div[1]/div/div/div[2]/div[2]/button")
        temp_list[-1].click()
        sleep(1)
        try:
            portal_id = []
            automation_id = driver.find_element_by_xpath("//a[contains(text(), 'Automation')]/../../..").get_attribute("data-rbd-draggable-id")
            portal_id = driver.find_elements_by_xpath("//*[contains(text(), 'Portal %s 2021 release')]/../.." % month)
            for each_portal_id in portal_id:
                if each_portal_id.get_attribute("data-rbd-draggable-id") == automation_id:
                    excute_parent = driver.find_element_by_xpath("//div[@data-rbd-draggable-id='%s']//div[@class='actions-wrapper executeRow']//a" % automation_id)
                    excute_parent.click()
                    sleep(20)
                    break
                else:
                    #print("else statement #################")
                    pass
        except Exception as erp:
            #print(erp)
            #print("automation not seen ######################")
            try:
                scr2 = driver.find_element_by_xpath('/html/body/div[5]/div/div/div[1]/div[1]/div[3]/div[2]')
            except NoSuchElementException:
                scr2 = driver.find_element_by_xpath('/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div[2]')
            sleep(1)
            driver.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", scr2)
            portal_id = []
            automation_id = driver.find_element_by_xpath("//a[contains(text(), 'Automation')]/../../..").get_attribute("data-rbd-draggable-id")
            portal_id = driver.find_elements_by_xpath("//*[contains(text(), 'Portal %s 2021 release')]/../.." % month)
            for each_portal_id in portal_id:
                if each_portal_id.get_attribute("data-rbd-draggable-id") == automation_id:
                    excute_parent = driver.find_element_by_xpath("//div[@data-rbd-draggable-id='%s']//div[@class='actions-wrapper executeRow']//a" % automation_id)
                    excute_parent.click()
                    sleep(10)
                else:
                    pass
        sleep(5)
        driver.switch_to.frame(driver.find_element_by_xpath("/html/body/div[1]/div[1]/div/div[3]/div[1]/div/div/iframe"))
        sleep(1)
        comment_box_text = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[2]/div/div/form/div[3]/div/div").click()
        comment_box_text_entry = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[2]/div/div/form/div[3]/div/div/div/div/div/div[2]/div/div/textarea")   
        comment_box_text_entry.send_keys(error)
        dropdown_selection = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[2]/div/div/form/div[1]/div[1]/div/div/div/div/span[3]/img").click()
        sleep(3)
        dropdown_selection = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[2]/div/div/form/div[1]/div[1]/div/div/div/div/span[3]/img").click()
        sleep(4)
        dropdown_selection_fail = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[2]/div/div/form/div[1]/div[1]/div/div/div/div/div/ul/li[2]").click()
        sleep(4)
        driver.close()
    except Exception as f:
        #print(f)
        #print("failed jira function")
        pass

def jira_function_pass_filter(month, file_name, result):
    try:
        print("entered jira pass function")
        s=Service(ChromeDriverManager().install())
        driver = webdriver.Chrome(service=s)
        driver.implicitly_wait(60)
        driver.maximize_window()
        sleep(1)
        driver.get("https://jmadevteam.atlassian.net/browse/"+file_name.split("\\")[-1])
        sleep(1)
        username_email_input = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div[2]/form/div[1]/div/div/div/input")
        username_email_input.click()
        username_email_input.send_keys("pselvarajan@blueally.com")
        continue_click = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div[2]/form/div[3]/button/span/span").click()
        password_email_input = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div[2]/form/div[2]/div/div/div/div/div/input")
        password_email_input.click()
        password_email_input.send_keys("Marines*012")
        login_click = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div[2]/div/section/div[2]/form/div[3]/button/span").click()
        sleep(10)
        try:
            scr1 = driver.find_element_by_xpath('/html/body/div[5]/div[1]/div/div[3]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]')
        except NoSuchElementException:
            scr1 = driver.find_element_by_xpath('/html/body/div[1]/div[1]/div/div[3]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]')
        sleep(1)
        driver.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", scr1)

        try:
            driver.switch_to.frame(driver.find_element_by_xpath("/html/body/div[5]/div[1]/div/div[3]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/div/div[5]/div[10]/div[3]/div[2]/div/iframe"))
        except NoSuchElementException:
            driver.switch_to.frame(driver.find_element_by_xpath("/html/body/div[1]/div[1]/div/div[3]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/div/div[5]/div[10]/div[3]/div[2]/div/iframe"))
        sleep(4)

        page_extend = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div[2]/div[1]/div/div[3]").click()
        sleep(2)
        temp_list = []
        temp_list = driver.find_elements_by_xpath("/html/body/div[1]/div/div/div[2]/div[2]/button")
        temp_list[-1].click()
        sleep(1)
        try:
            portal_id = []
            automation_id = driver.find_element_by_xpath("//a[contains(text(), 'Automation')]/../../..").get_attribute("data-rbd-draggable-id")
            portal_id = driver.find_elements_by_xpath("//*[contains(text(), 'Portal %s 2021 release')]/../.." % month)
            for each_portal_id in portal_id:
                if each_portal_id.get_attribute("data-rbd-draggable-id") == automation_id:
                    excute_parent = driver.find_element_by_xpath("//div[@data-rbd-draggable-id='%s']//div[@class='actions-wrapper executeRow']//a" % automation_id)
                    excute_parent.click()
                    sleep(20)
                    break
                else:
                    pass
        except Exception as erp:
            try:
                scr2 = driver.find_element_by_xpath('/html/body/div[5]/div/div/div[1]/div[1]/div[3]/div[2]')
            except NoSuchElementException:
                scr2 = driver.find_element_by_xpath('/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div[2]')
            sleep(1)
            driver.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", scr2)
            portal_id = []
            automation_id = driver.find_element_by_xpath("//a[contains(text(), 'Automation')]/../../..").get_attribute("data-rbd-draggable-id")
            portal_id = driver.find_elements_by_xpath("//*[contains(text(), 'Portal %s 2021 release')]/../.." % month)
            for each_portal_id in portal_id:
                if each_portal_id.get_attribute("data-rbd-draggable-id") == automation_id:
                    excute_parent = driver.find_element_by_xpath("//div[@data-rbd-draggable-id='%s']//div[@class='actions-wrapper executeRow']//a" % automation_id)
                    excute_parent.click()
                    sleep(10)
                else:
                    pass
        sleep(5)
        driver.switch_to.frame(driver.find_element_by_xpath("/html/body/div[1]/div[1]/div/div[3]/div[1]/div/div/iframe"))
        sleep(1)
        comment_box_text = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[2]/div/div/form/div[3]/div/div").click()
        comment_box_text_entry = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[2]/div/div/form/div[3]/div/div/div/div/div/div[2]/div/div/textarea")   
        comment_box_text_entry.send_keys(result)
        dropdown_selection = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[2]/div/div/form/div[1]/div[1]/div/div/div/div/span[3]/img").click()
        sleep(3)
        dropdown_selection = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[2]/div/div/form/div[1]/div[1]/div/div/div/div/span[3]/img").click()
        sleep(4)
        dropdown_selection_pass = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[2]/div/div/form/div[1]/div[1]/div/div/div/div/div/ul/li[1]").click()
        sleep(4)
        driver.close()
    except Exception as f:
        #print(f)
        #print("failed jira function")
        pass