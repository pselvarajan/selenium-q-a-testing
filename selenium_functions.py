from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import requests
from selenium.webdriver.common.action_chains import ActionChains
from time import strftime
import sys
import jira_function
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import NoSuchElementException
import datetime

def var_gather():
    global file_name
    file_name = sys.argv[0][:-3]
    return file_name

def login_portal(month, file_name, driver):
    try:
        driver.get("http://portal2.preprod.jma-it.com/login")
    except:
        jira_function.jira_function_fail_filter(month, file_name, "Portal not reachable.\nCheck VPN connection.\nTest Step 1 failed.")
        driver.close()
        quit()

def login_portal_with_username_password(month, file_name, username, password, driver):
    try:
        username_input = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/input[1]")
        password_input = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/input[2]")
        sign_in_input = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/button/span")
        username_input.click()
        username_input.send_keys(username)
        password_input.click()
        password_input.send_keys(password)
        sign_in_input.click()
        sleep(1)
    except:
        jira_function.jira_function_fail_filter(month, file_name, "Cannot find username/password/sign in element.\nTest Step 1 failed.")
        driver.close()
        quit()

def login_portal_error(month, file_name, driver):
    try:
        login_error = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/div").text
        jira_function.jira_function_fail_filter(month, file_name, "The username/password entered is incorrect.\nTest Step 1 failed.")
        driver.close()
        quit()
    except Exception as f:
        print(f)
        pass

def switch_map_node_locator(month, file_name, driver):
    try:
        #global switch_element_1
        global switch_element_2
        global switch_element_3
        global switch_element_4
        switch_map = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[1]/div[1]/span[2]/a/i").click()
        sleep(8)
        view_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/p-toolbar[2]/div/div/div[1]/p-dropdown/div/label").click()
        vzb_select = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/p-toolbar[2]/div/div/div[1]/p-dropdown/div/div[3]/div/ul/p-dropdownitem[1]/li").click()
        minimize_map = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/div/div[1]/div/div[2]/div[1]/div/a[2]").click()
        sleep(5)
        #switch_element_1 = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/div/div[1]/div/div[1]/div[4]/div[37]")
        switch_element_2 = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/div/div[1]/div/div[1]/div[4]/div[3]")
        switch_element_3 = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/div/div[1]/div/div[1]/div[4]/div[16]")
        switch_element_4 = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/div/div[1]/div/div[1]/div[4]/div[1]")
    except:
        jira_function.jira_function_fail_filter(month, file_name, "Switch map button not visible and clickable\nview menu is not visible and clickable\nnodes on map not visible and clickable.\nTest Step 2 failed.")
        driver.close()
        quit()

def switch_map_node_selector(month, file_name, driver):
    try:
        action_class_init = ActionChains(driver)
        #action_class_init.move_to_element(switch_element_1).perform()
        sleep(3)
        CLLI_find = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/div/div[1]/div/div[1]/div[6]").text
        CLLI_find_1 = CLLI_find.splitlines()[4:6]
        action_class_init.move_to_element(switch_element_2).perform()
        sleep(1)
        CLLI_find = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/div/div[1]/div/div[1]/div[6]").text
        CLLI_find_2 = CLLI_find.splitlines()[4:6]
        action_class_init.move_to_element(switch_element_3).perform()
        sleep(1)
        CLLI_find = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/div/div[1]/div/div[1]/div[6]").text
        CLLI_find_3 = CLLI_find.splitlines()[4:6]
        action_class_init.move_to_element(switch_element_4).perform()
        sleep(1)
        CLLI_find = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/div/div[1]/div/div[1]/div[6]").text
        CLLI_find_4 = CLLI_find.splitlines()[4:6]
        jira_function.jira_function_pass_filter(month, file_name, "Building CLLI elements visible.\n"+"\n"+CLLI_find_2[1]+"\n"+CLLI_find_3[1]+"\n"+CLLI_find_4[1]+"\n")
        driver.close()
        quit()
    except Exception as t:
        print(t)
        jira_function.jira_function_fail_filter(month, file_name, "Building CLLI missing for nodes.\nTest Step 3 failed.")
        driver.close()
        quit()

def templates_timelines_duplicate_6954(month, file_name, driver):
    try:
        templates_map = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[1]/div[1]/span[3]").click()
        timelines_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/p-menubar/div/p-menubarsub/ul/li[2]/a/span").click()
        add_template_select = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/p-toolbar/div/div[2]/div/button/span[2]").click()
        input_field = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/div/div[1]/div/input")
        input_field.click()
        input_field.send_keys("PIP NNI")
        milestone_input_field = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[1]/td[3]/input")
        milestone_input_field.click()
        milestone_input_field.send_keys("Milestone_Name")
        task_input_field = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr/td[3]/input")
        task_input_field.click()
        task_input_field.send_keys("Task_Name")
        save_button = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/footer/div/button[1]/span[2]").click()
        sleep(5)
        if input_field.get_attribute("class") == "ng-valid ui-inputtext ui-corner-all ui-state-default ui-widget ng-dirty ui-state-filled ng-touched ng-invalid":
            jira_function.jira_function_pass_filter(month, file_name, "Unable to save due to duplicate name")
            driver.close()
            quit()
        else:
            jira_function.jira_function_fail_filter(month, file_name, "Template created, error this testing case should not be able to create a template will duplicate names")
            driver.close()
            quit()
    except Exception as q:
        print(t)
        jira_function.jira_function_fail_filter(month, file_name, "Template map not visible or clickable.\nTimelines tab not visible and clickable.\nAdd template button not visible and clickable.\nInput field not visible and clickable.\nNot able to enter data in fields.\nSave button not working.")
        driver.close()
        quit()

def templates_port_2596(month, file_name, driver):
    try:
        templates_map = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[1]/div[1]/span[3]").click()
        ports_tab = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/p-menubar/div/p-menubarsub/ul/li[3]").click()
        tools_tab = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-ports/p-card/div/div/div[1]/p-tabview/div/ul/li[2]").click()
        tools_add_button = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-ports/p-card/div/div/div[2]/p-footer/button[1]/span[2]").click()
        input_field = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-ports/templates-ports-tools/form/fieldset[1]/div[1]/input")
        input_field.click()
        input_field.send_keys("Test Template 1")
        Template_Type_Select = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-ports/templates-ports-tools/form/fieldset[1]/div[2]/p-dropdown/div/label").click()
        Template_Type_Click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-ports/templates-ports-tools/form/fieldset[1]/div[2]/p-dropdown/div/div[3]/div/ul/p-dropdownitem[1]/li/span").click()
        Template_Type_Save= driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-ports/templates-ports-tools/form/footer/div/button[2]/span[2]").click()
        sleep(500)
        #add_template_select = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/p-toolbar/div/div[2]/div/button/span[2]").click()
        #input_field = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/div/div[1]/div/input")
        #input_field.click()
        #input_field.send_keys("PIP NNI")
        #milestone_input_field = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[1]/td[3]/input")
        #milestone_input_field.click()
        #milestone_input_field.send_keys("Milestone_Name")
        #task_input_field = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr/td[3]/input")
        #task_input_field.click()
        #task_input_field.send_keys("Task_Name")
        #save_button = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/footer/div/button[1]/span[2]").click()
        #sleep(5)
        #if input_field.get_attribute("class") == "ng-valid ui-inputtext ui-corner-all ui-state-default ui-widget ng-dirty ui-state-filled ng-touched ng-invalid":
        #    jira_function.jira_function_pass_filter(month, file_name, "Unable to save due to duplicate name")
        #    driver.close()
        #    quit()
        #else:
        #    jira_function.jira_function_fail_filter(month, file_name, "Template created, error this testing case should not be able to create a template will duplicate names")
        #    driver.close()
        #    quit()
    except Exception as q:
        print(t)
        jira_function.jira_function_fail_filter(month, file_name, "Template map not visible or clickable.\nTimelines tab not visible and clickable.\nAdd template button not visible and clickable.\nInput field not visible and clickable.\nNot able to enter data in fields.\nSave button not working.")
        driver.close()
        quit()

def test_case_975(month, file_name, driver):
    try:
        node_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[1]/div[1]/span[1]/a/i")
        node_input.click()
        node_filter_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input")
        node_filter_input.click()
        node_filter_input.send_keys("A50SN")
        empty_list = []
        empty_list = driver.find_elements_by_xpath("//td[@class='node ng-star-inserted']")
        for each_empty_list in empty_list:
            if each_empty_list.text[0:5] == "A50SN":
                pass
            else:
                jira_function.jira_function_fail_filter(month, file_name, "Error, filtered A50SN but got "+each_empty_list.text[0:])
                quit()
        empty_list.clear()
        driver.find_element_by_xpath('/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input').clear()
        node_filter_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input")
        node_filter_input.click()
        node_filter_input.send_keys("ALBY")
        empty_list = driver.find_elements_by_xpath("//td[@class='node ng-star-inserted']")
        for each_empty_list in empty_list:
            if each_empty_list.text[0:4] == "ALBY":
                pass
            else:
                jira_function.jira_function_fail_filter(month, file_name, "Error, filtered ALBY but got "+each_empty_list.text[0:])
                quit()
        empty_list.clear()
        driver.find_element_by_xpath('/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input').clear()
        node_filter_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input")
        node_filter_input.click()
        node_filter_input.send_keys("ANNP")
        empty_list = driver.find_elements_by_xpath("//td[@class='node ng-star-inserted']")
        for each_empty_list in empty_list:
            if each_empty_list.text[0:4] == "ANNP":
                pass
            else:
                jira_function.jira_function_fail_filter(month, file_name, "Error, filtered ANNP but got "+each_empty_list.text[0:])
                quit()
        empty_list.clear()
        driver.find_element_by_xpath('/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input').clear()
        node_filter_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input")
        node_filter_input.click()
        node_filter_input.send_keys("ARTN")
        empty_list = driver.find_elements_by_xpath("//td[@class='node ng-star-inserted']")
        for each_empty_list in empty_list:
            if each_empty_list.text[0:4] == "ARTN":
                pass
            else:
                jira_function.jira_function_fail_filter(month, file_name, "Error, filtered ARTN but got "+each_empty_list.text[0:])
                quit()
        empty_list.clear()
        driver.find_element_by_xpath('/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input').clear()
        node_filter_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input")
        node_filter_input.click()
        node_filter_input.send_keys("ASPK")
        empty_list = driver.find_elements_by_xpath("//td[@class='node ng-star-inserted']")
        for each_empty_list in empty_list:
            if each_empty_list.text[0:4] == "ASPK":
                pass
            else:
                jira_function.jira_function_fail_filter(month, file_name, "Error, filtered ASPK but got "+each_empty_list.text[0:])
                quit()
        empty_list.clear()
        driver.find_element_by_xpath('/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input').clear()
        node_filter_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input")
        node_filter_input.click()
        node_filter_input.send_keys("ATL5")
        empty_list = driver.find_elements_by_xpath("//td[@class='node ng-star-inserted']")
        for each_empty_list in empty_list:
            if each_empty_list.text[0:4] == "ATL5":
                pass
            else:
                jira_function.jira_function_fail_filter(month, file_name, "Error, filtered ATL5 but got "+each_empty_list.text[0:])
                quit()
        empty_list.clear()
        
        #driver.find_element_by_xpath('/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input').clear()
        #node_filter_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input")
        #node_filter_input.click()
        #node_filter_input.send_keys("ATLC")
        #empty_list = driver.find_elements_by_xpath("//td[@class='node ng-star-inserted']")
        #for each_empty_list in empty_list:
        #    if each_empty_list.text[0:3] == "ATLC":
        #        pass
        #    else:
        #        jira_function.jira_function_fail_filter(month, file_name, "Error, filtered ATLC but got "+each_empty_list.text[0:])
        #        quit()
        #empty_list.clear()
        
        driver.find_element_by_xpath('/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input').clear()
        node_filter_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-node-list/div/div/p-toolbar[2]/div/div[1]/div[2]/input")
        node_filter_input.click()
        node_filter_input.send_keys("AUB1")
        empty_list = driver.find_elements_by_xpath("//td[@class='node ng-star-inserted']")
        for each_empty_list in empty_list:
            if each_empty_list.text[0:4] == "AUB1":
                pass
            else:
                jira_function.jira_function_fail_filter(month, file_name, "Error, filtered AUB1 but got "+each_empty_list.text[0:])
                quit() 
            #print(each_empty_list.text)
        #for e in empty_list:
        #print(empty_list.text)
        #node_photo_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/p-menubar/div/p-menubarsub/ul/li[3]/a")
        #node_photo_input.click()
        #node_album_view = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-photos/p-menubar/div/p-menubarsub/ul/li[2]/a/span")
        #node_album_view.click()
        #node_album_edit_view = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-photos/node-albums-view/div[1]/p-toolbar/div/div[2]/div[3]/button/span[2]")
        #node_album_edit_view.click()
        jira_function.jira_function_pass_filter(month, file_name, "Nodes button visible and clickable.\nExample nodes(A50SN,ALBY,ANNP,ARTN,ASPK,ATL5,AUB1) is visible when searching.")
        #print()
    except Exception as r:
        print(r)
        jira_function.jira_function_fail_filter(month, file_name, "failing")
        #print("\nCannot find node/node_example/node_diagram button.")
        quit()

def test_case_5237(month, file_name, driver):
    try:
        node_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[1]/div[1]/span[1]/a/i").click()
        node_example_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-node-list/div/app-node-browser/div/div/p-table/div/div/div/div[2]/table/tbody/tr[1]/td[2]/a").click()
        node_data_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/p-menubar/div/p-menubarsub/ul/li[5]/a/span[1]")
        action_class_init = ActionChains(driver)
        action_class_init.move_to_element(node_data_input).perform()
        sleep(5)
        node_notes_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/p-menubar/div/p-menubarsub/ul/li[5]/p-menubarsub/ul/li[15]/a").click()
        sleep(3)
        #
        add_row_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[1]/p-toolbar[2]/div/div[2]/div[1]/button").click()
        enter_note_name = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/tbody/tr[1]/td[1]/input")
        enter_note_name.click()
        enter_note_name.send_keys("Testing Notes Names")
        save_button = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/footer/div/button[1]/span[2]").click()
        #
        sleep(2)
        temp_dict = {"name": [],"username": [],"date": []}
        temp_list_note_name = []
        temp_list_note_name = driver.find_elements_by_xpath("/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/tbody/tr/td")
        sleep(3)
        for each_temp_list_note_name in temp_list_note_name[::3]:
            temp_dict['name'].append(each_temp_list_note_name.text)
        for each_temp_list_note_username in temp_list_note_name[1::3]:
            temp_dict['username'].append(each_temp_list_note_username.text)
        for each_temp_list_note_date in temp_list_note_name[2::3]:
            temp_dict['date'].append(each_temp_list_note_date.text.split(' ')[2])

        sorted_temp_dict_name = sorted(temp_dict['name'], key=lambda v: v.upper())
        sorted_temp_dict_username = sorted(temp_dict['username'], key=lambda v: v.upper())
        sorted_temp_dict_date = sorted(temp_dict['date'])
        reverse_temp_dict_name = sorted(temp_dict['name'],reverse=True, key=lambda v: v.upper())
        reverse_temp_dict_username = sorted(temp_dict['username'],reverse=True, key=lambda v: v.upper())
        reverse_temp_dict_date = sorted(temp_dict['date'],reverse=True)

        temp_list_note_name.clear()
        click_note_name_sort = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/thead/tr/th[1]/p-sorticon/i").click()
        temp_list_note_name = driver.find_elements_by_xpath("/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/tbody/tr/td")
        sleep(1)
        temp_list_note_name_new = []
        for each_temp_list_note_name_1 in temp_list_note_name[::3]:
           temp_list_note_name_new.append(each_temp_list_note_name_1.text)

        if temp_list_note_name_new == sorted_temp_dict_name:
            pass
        else:
            jira_function.jira_function_fail_filter(month, file_name, "Note column is not sorted in ascending order")

        
        click_note_name_reverse = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/thead/tr/th[1]/p-sorticon/i").click()
        temp_list_note_name.clear()
        temp_list_note_name = driver.find_elements_by_xpath("/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/tbody/tr/td")
        sleep(1)
        temp_list_note_name_new.clear()
        for each_temp_list_note_name_2 in temp_list_note_name[::3]:
           temp_list_note_name_new.append(each_temp_list_note_name_2.text)
        if temp_list_note_name_new == reverse_temp_dict_name:
            pass
        else:
            jira_function.jira_function_fail_filter(month, file_name, "Note column is not reversed in descending order")

        click_note_username_sorted = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/thead/tr/th[2]/p-sorticon").click()
        temp_list_note_name.clear()
        temp_list_note_name = driver.find_elements_by_xpath("/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/tbody/tr/td")
        sleep(1)
        temp_list_note_name_new.clear()
        for each_temp_list_note_name_3 in temp_list_note_name[1::3]:
           temp_list_note_name_new.append(each_temp_list_note_name_3.text)
        if temp_list_note_name_new == sorted_temp_dict_username:
            pass
        else:
            jira_function.jira_function_fail_filter(month, file_name, "Username column is not sorted in ascending order")

        click_note_username_reverse = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/thead/tr/th[2]/p-sorticon").click()
        temp_list_note_name.clear()
        temp_list_note_name = driver.find_elements_by_xpath("/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/tbody/tr/td")
        sleep(1)
        temp_list_note_name_new.clear()
        for each_temp_list_note_name_4 in temp_list_note_name[1::3]:
           temp_list_note_name_new.append(each_temp_list_note_name_4.text)
        if temp_list_note_name_new == reverse_temp_dict_username:
            pass
        else:
            jira_function.jira_function_fail_filter(month, file_name, "Username column is not reversed in descending order")

        click_note_date_sorted = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/thead/tr/th[3]/p-sorticon").click()
        temp_list_note_name.clear()
        temp_list_note_name = driver.find_elements_by_xpath("/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/tbody/tr/td")
        sleep(1)
        temp_list_note_name_new.clear()
        for each_temp_list_note_name_5 in temp_list_note_name[2::3]:
           temp_list_note_name_new.append(each_temp_list_note_name_5.text.split(' ')[2])
        temp_list_note_name_new=sorted(temp_list_note_name_new)
        if temp_list_note_name_new == sorted_temp_dict_date:
            pass
        else:
            jira_function.jira_function_fail_filter(month, file_name, "Date column is not sorted in ascending order")

        click_note_date_reverse = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/thead/tr/th[3]/p-sorticon").click()
        temp_list_note_name.clear()
        temp_list_note_name = driver.find_elements_by_xpath("/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/tbody/tr/td")
        sleep(1)
        temp_list_note_name_new.clear()
        for each_temp_list_note_name_6 in temp_list_note_name[2::3]:
           temp_list_note_name_new.append(each_temp_list_note_name_6.text.split(' ')[2])
        temp_list_note_name_new=sorted(temp_list_note_name_new,reverse=True)
        if temp_list_note_name_new == reverse_temp_dict_date:
            pass
        else:
            jira_function.jira_function_fail_filter(month, file_name, "Date column is not reversed in descending order")
        jira_function.jira_function_pass_filter(month, file_name, "Added a row in Notes.\nSorting of columns successful.")
    except Exception as r:
        print(r)
        jira_function.jira_function_fail_filter(month, file_name, "Cannot find node/node_example/node_diagram button.")

def test_case_6193(month, file_name, driver):
    try:
        templates_map = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[1]/div[1]/span[3]").click()
        timelines_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/p-menubar/div/p-menubarsub/ul/li[2]/a/span").click()
        template_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[1]/p-card/div/div/div/p-listbox/div/div[2]/ul/li[3]/a").click()
        template_edit_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/p-toolbar/div/div[2]/div[1]/button/span[2]").click()
        sleep(2)
        template_edit_add_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr[1]/td[9]/a[2]").click()
        template_edit_add_enter = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr[2]/td[3]/input")
        template_edit_add_enter.click()
        template_edit_add_enter.clear()
        template_edit_add_enter.send_keys("New Task Test 1")
        template_edit_add_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr[1]/td[9]/a[2]").click()
        template_edit_add_enter_second = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr[2]/td[3]/input")
        template_edit_add_enter_second.click()
        template_edit_add_enter_second.clear()
        template_edit_add_enter_second.send_keys("New Task Test 2")
        save_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/footer/div/button[1]/span[2]").click()
        sleep(5)
        template_edit_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/p-toolbar/div/div[2]/div[1]/button/span[2]").click()
        template_edit_select_first = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr[2]/td[1]/label/p-checkbox").click()
        template_edit_select_second = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr[3]/td[1]/label/p-checkbox").click()
        cancel_check = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/footer/div/button[1]/span[2]").text
        delete_check = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/footer/div/button[2]/span[2]").text
        sleep(3)
        if cancel_check == "Cancel" and delete_check == "Delete":
            pass
        else:
            jira_function.jira_function_fail_filter(month, file_name, "Cancel/Delete button not visible.")
        cancel_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/footer/div/button[1]/span[2]").click()
        save_check = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/footer/div/button[1]/span[2]").text
        cancel_check = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/footer/div/button[2]/span[2]").text
        sleep(3)
        if save_check == "Save" and cancel_check == "Cancel":
            pass
        else:
            jira_function.jira_function_fail_filter(month, file_name, "Save/Cancel button not visible.")
        sleep(3)
        cancel_edit_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/footer/div/button[2]/span[1]").click()
        cancel_click_confirm = driver.find_element(By.XPATH, "/html/body/div[1]/div[3]/p-footer/button[2]/span[2]").click()
        sleep(3)
        if "New Task Test 1" and "New Task Test 2" in driver.page_source:
            pass
        else:
            jira_function.jira_function_fail_filter(month, file_name, "New Task Test 1 and New Task Test 2 is not present in the page")
        sleep(5)
        template_edit_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/p-toolbar/div/div[2]/div[1]/button/span[2]").click()
        template_edit_select_first = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr[2]/td[1]/label/p-checkbox").click()
        template_edit_select_second = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr[3]/td[1]/label/p-checkbox").click()
        delete_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/footer/div/button[2]/span[2]").click()
        delete_click_confirm = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/p-confirmdialog[3]/div/div[3]/button[1]/span[2]").click()
        save_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/footer/div/button[1]/span[2]").click()
        if "New Task Test 1" and "New Task Test 2" in driver.page_source:
            jira_function.jira_function_fail_filter(month, file_name, "New Task Test 1 and New Task Test 2 is present in the page, error.")
        else:
            pass
        sleep(5)
        template_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[1]/p-card/div/div/div/p-listbox/div/div[2]/ul/li[2]/a").click()
        sleep(5)
        template_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[1]/p-card/div/div/div/p-listbox/div/div[2]/ul/li[3]/a").click()
        sleep(5)
        if "New Task Test 1" and "New Task Test 2" in driver.page_source:
            jira_function.jira_function_fail_filter(month, file_name, "Moved to C20 LD, and back to C20 Local New Task Test 1,New Task Test 2 does exist. Error, the steps should not exist.")
        else:
            pass
        jira_function.jira_function_pass_filter(month, file_name, "PV2VERIZON-6193 passed all test cases.")
    except:
        jira_function.jira_function_fail_filter(month, file_name, "Failed")

def test_case_3078(month, file_name, driver):
    try:
        node_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[1]/div[1]/span[1]/a/i").click()
        node_example_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-node-list/div/app-node-browser/div/div/p-table/div/div/div/div[2]/table/tbody/tr[1]/td[2]/a").click()
        sleep(100)
        #node_data_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/p-menubar/div/p-menubarsub/ul/li[5]/a/span[1]")
        #action_class_init = ActionChains(driver)
        #action_class_init.move_to_element(node_data_input).perform()
        #sleep(5)
        #node_notes_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/p-menubar/div/p-menubarsub/ul/li[5]/p-menubarsub/ul/li[15]/a").click()
        #sleep(3)
        ##
        #add_row_click = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[1]/p-toolbar[2]/div/div[2]/div[1]/button").click()
        #enter_note_name = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/nodes/div/div/node-data/node-history-notes/div/div[2]/form/p-table/div/div/table/tbody/tr[1]/td[1]/input")
        #enter_note_name.click()
        #enter_note_name.send_keys("Testing Notes Names")
    except:
        print("failed")