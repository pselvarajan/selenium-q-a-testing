from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from time import strftime
import sys
import jira_function
import requests
from selenium_functions import *

file_name = var_gather()
month = "December"

def main():
    s=Service(ChromeDriverManager().install())
    driver = webdriver.Chrome(service=s)
    driver.implicitly_wait(10)
    driver.maximize_window()
    
    login_portal(month, file_name, driver)
    login_portal_with_username_password(month, file_name, "superadmintest1", "Jma123##", driver)
    login_portal_error(month, file_name, driver)
    test_case_6193(month, file_name, driver)

main()