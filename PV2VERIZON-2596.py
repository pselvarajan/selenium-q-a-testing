from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import requests
import Chrome_webdriver_download_check
from selenium.webdriver.common.action_chains import ActionChains

driver = webdriver.Chrome(service=Chrome_webdriver_download_check.s)
driver.implicitly_wait(5)
driver.maximize_window()

try:
    driver.get("http://portal2.preprod.jma-it.com/login")
    print("\nLog on to the Portal as Capacity Planning.")
except Exception as f:
    print(f)
    print("\nGet request failed.\nPlease check VPN connection.\n")
    quit()

try:
    username_input = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/input[1]")
    password_input = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/input[2]")
    sign_in_input = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/button/span")
    username_input.click()
    username_input.send_keys("cptest1")
    password_input.click()
    password_input.send_keys("Jma123##")
    sign_in_input.click()
    print("\nPortal opens.\nusername element visible and clickable, username text sent.\npassword element visible and clickable, password text sent.\nsign in element visible and clickable.")
    try:
        login_error = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/div").text
        if login_error == "The username or password entered is incorrect.":
            print("\nThe username/password entered is incorrect.")
            quit()
        else:
            print("\nThe username/password entered is correct.")
    except Exception as x:
        print(x)
        print("\nThe username/password entered is correct.")
except Exception as e:
    print(e)
    print("\nCannot find username/password/sign in button.\nFailed to click the elements.\nFailed to enter input.")
    quit()

try:
    templates_map = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[1]/div[1]/span[3]").click()
    ports_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/p-menubar/div/p-menubarsub/ul/li[3]/a/span").click()
    add_template_select = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/p-toolbar/div/div[2]/div/button/span[2]").click()
    input_field = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/div/div[1]/div/input")
    input_field.click()
    input_field.send_keys("PIP NNI 2")
    milestone_input_field = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[1]/td[3]/input")
    milestone_input_field.click()
    milestone_input_field.send_keys("Milestone_Name")
    task_input_field = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/div[2]/templates-timeline-tasks/div/p-table/div/div/div/div[2]/table/tbody/tr[2]/td/div/p-table/div/div/table/tbody/tr/td[3]/input")
    task_input_field.click()
    task_input_field.send_keys("Task_Name")
    save_button = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/templates/div/templates-timelines/form/div/footer/div/button[1]/span[2]").click()
    if input_field.get_attribute("class") == "ng-valid ui-inputtext ui-corner-all ui-state-default ui-widget ng-dirty ui-state-filled ng-touched ng-invalid":
        print("Unable to save due to duplicate name")
        quit()
    else:
        print("Template created, error this testing case should not be able to create a template will duplicate names")
    sleep(1)
    print("\nTemplate map not visible or clickable.\nTimelines tab not visible and clickable.\nAdd template button not visible and clickable.\nInput field not visible and clickable.\nNot able to enter data in fields.\nSave button not working.")
except Exception as q:
    print(q)
    print("\nCannot find node/node_example/node_diagram button.")
    quit()