from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import requests
import Chrome_webdriver_download_check
from selenium.webdriver.common.action_chains import ActionChains
from time import strftime

driver = webdriver.Chrome(service=Chrome_webdriver_download_check.s)
driver.implicitly_wait(10)
driver.maximize_window()

try:
    driver.get("http://portal2.preprod.jma-it.com/login")
    print("\nLog on to the Portal as Capacity Planning.")
except:
    print("\nGet request failed.\nPlease check VPN connection.\n")
    quit()

try:
    username_input = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/input[1]")
    password_input = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/input[2]")
    sign_in_input = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/button/span")
    username_input.click()
    username_input.send_keys("CapacityEng1")
    password_input.click()
    password_input.send_keys("Jma123##")
    sign_in_input.click()
    print("\nPortal opens.\nusername element visible and clickable, username text sent.\npassword element visible and clickable, password text sent.\nsign in element visible and clickable.")
    try:
        login_error = driver.find_element(By.XPATH, "/html/body/app-root/div/div/ng-component/div/div/div/form/div[2]/div").text
        if login_error == "The username or password entered is incorrect.":
            print("\nThe username/password entered is incorrect.")
            quit()
        else:
            print("\nThe username/password entered is correct.")
    except Exception as x:
        print(x)
        print("\nThe username/password entered is correct.")
except Exception as e:
    print(e)
    print("\nCannot find username/password/sign in button.\nFailed to click the elements.\nFailed to enter input.")
    quit()

try:
    switch_map = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[1]/div[1]/span[2]/a/i").click()
    sleep(5)
    region_input = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/p-toolbar[2]/div/div/div[2]/p-dropdown/div/label").click()
    region_select = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/p-toolbar[2]/div/div/div[2]/p-dropdown/div/div[3]/div/ul/p-dropdownitem[2]/li/span").click()
    try:
        network_type_text = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/p-toolbar[2]/div/div/div[3]/label")
        network_type_text_data = driver.find_element(By.XPATH, "/html/body/app-root/div[2]/div[2]/app-vzb-switch-map/div/p-toolbar[2]/div/div/div[3]/p-dropdown/div/label")
        print("Network type is visible")
    except:
        print("Network type is not visible")
        quit()
except:
    print("\nSwitch map button not visible and clickable\nview menu is not visible and clickable\nnodes on map not visible and clickable")
    quit()